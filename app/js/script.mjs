const t = document.querySelector('#profile-info');
let t2 = document.querySelector('#profile-edit');


const profileInfoMain = t.content.querySelector('.profile-info-main');
const profileBgContent = t.content.querySelector('.profile-bg-content');


const profileBg = t.content.querySelector('.profile-bg');
const profileAvatar = t.content.querySelector('.profile-avatar');
const profileName = t.content.querySelector('.profile-name');
const profileSpecial = t.content.querySelector('.profile-special');
const profileDistrict = t.content.querySelector('.profile-district');
const profileRedact = t.content.querySelector('.profile-redact');


const clockThing = document.querySelector('.clock');
const thing1 = document.querySelector('.thing1');
const thing2 = document.querySelector('.thing2');
const thing3 = document.querySelector('.thing3');
const pageNav = document.querySelector('.page-nav');


const swapContent = document.querySelector('.swap-content');
const feedContent = document.querySelector('.feed-content');


const toProfile = document.querySelector('.to-profile');
const toFeed = document.querySelector('.logo');


const profileEditMain = t2.content.querySelector('.profile-edit-main');
const profileEditAvatar = t2.content.querySelector('.profile-edit-avatar');
const profileEditName = t2.content.querySelector('.profile-edit-name');
const profileEditDistrict = t2.content.querySelector('.profile-edit-district');
const profileEditSpecial = t2.content.querySelector('.profile-edit-special');
const profileEditButton = t2.content.querySelector('.profile-edit-save');


swapContent.appendChild(profileBgContent);
swapContent.appendChild(profileInfoMain);
swapContent.appendChild(profileEditMain);


profileBgContent.classList.add('profile-parameters');
profileInfoMain.classList.add('profile-parameters');
profileBg.classList.add('profile-parameters');


function profileAppear() {
  profileBgContent.classList.remove('hidden');
  profileInfoMain.classList.remove('hidden');
  feedContent.classList.add('hidden');
  clockThing.classList.add('hidden');
  thing1.classList.add('hidden');
  thing2.classList.add('hidden');
  thing3.classList.add('hidden');
  profileEditMain.classList.add('hidden');
  pageNav.style.marginTop = `-25px`;
}

function feedAppear() {
  profileBgContent.classList.add('hidden');
  profileInfoMain.classList.add('hidden');
  feedContent.classList.remove('hidden');
  clockThing.classList.remove('hidden');
  thing1.classList.remove('hidden');
  thing2.classList.remove('hidden');
  thing3.classList.remove('hidden');
  profileEditMain.classList.add('hidden');
  pageNav.style.marginTop = `25px`;
}


function redactProfileAppear() {
  profileBgContent.classList.add('hidden');
  profileInfoMain.classList.add('hidden');
  feedContent.classList.add('hidden');
  profileEditMain.classList.remove('hidden');
}

function redactProfile() {
  // localStorage.setItem('profileAvatar', profileEditAvatar.value);
  // localStorage.setItem('profileBg', profileEdit)
  localStorage.setItem('profileName', profileEditName.value);
  localStorage.setItem('profileDistrict', profileEditDistrict.value);
  localStorage.setItem('profileSpecial', profileEditSpecial.value);


  profileName.innerHTML = localStorage.getItem('profileName');
  profileDistrict.innerHTML = localStorage.getItem('profileDistrict');
  profileSpecial.innerHTML = localStorage.getItem('profileSpecial');
}

// profileBg.src = localStorage.getItem('profileBg');
// profileAvatar.src = localStorage.getItem('profileAvatar');

profileDistrict.innerHTML = localStorage.getItem('profileDistrict');
profileSpecial.innerHTML = localStorage.getItem('profileSpecial');
profileName.innerHTML = localStorage.getItem('profileName');


profileAvatar.src = 'assets/img/ava.svg';
profileRedact.innerHTML = 'Редактировать';
profileBg.src = 'assets/img/profile.svg';


document.body.appendChild(t.content);
document.body.appendChild(t2.content);


profileEditButton.addEventListener('click', redactProfile);
profileRedact.addEventListener('click', redactProfileAppear);
toFeed.addEventListener('click', feedAppear);
toProfile.addEventListener('click', profileAppear);


// time

let clock = document.querySelector('.clock');

function update() {

  let date = new Date();
  let hours = date.getHours();
  if (hours < 10) hours = '0' + hours;
  clock.children[0].innerHTML = hours;

  let minutes = date.getMinutes();
  if (minutes < 10) minutes = '0' + minutes;
  clock.children[1].innerHTML = minutes;
}

update();
setInterval(update, 1000);

// black theme

'use strict';
// создаем <link rel="stylesheet" href="light|dark.css">
let head = document.head,
  link = document.createElement('link');
link.rel = 'stylesheet';
// проверяем значение из localStorage если dark то темная тема
if (localStorage.getItem('themeStyle') === 'dark') {
  link.href = 'css/dark.css'; // ссылка на темный стиль
  document.getElementById('switch-1').setAttribute('checked', true); // переключаем чекбокс в положение "темная тема"
}
// по умолчанию светлая тема
else {
  link.href = 'css/light.css'; // ссылка на светлый стиль
}
head.appendChild(link); // вставляем <link rel="stylesheet" href="light|dark.css"> в шапку страницы между темаги head

// событие при переключении чекбокса
document.getElementById('switch-1').addEventListener('change', ev => {
  let btn = ev.target;
  // если чекбокс включен
  if (btn.checked) {
    link.href = 'css/dark.css'; // сключаем темную тему
    localStorage.setItem('themeStyle', 'dark'); // записываем значение в localStorage
  } else {
    link.href = 'css/light.css'; // включаем светлую тему
    localStorage.setItem('themeStyle', 'light'); // записываем значение в localStorage
  }
});

// post button


const writePostButton = document.querySelector('.write-post');
const close = document.querySelector('.close');
const modal = document.querySelector('.modal');
const flex = document.querySelector('.flex');

function writePost() {
  flex.style = `filter: blur(2px)`;
  modal.classList.remove('hidden');
}

function writePostHide() {
  flex.style = `filter: blur(0px)`;
  modal.classList.add('hidden');
}

close.addEventListener('click', writePostHide);
writePostButton.addEventListener('click', writePost);


// post




// elements.push(postText.textContent);














let elements = [];
const postBtn = document.querySelector('.post-btn');

function renderSomething(elements) {
  elements = elements;
  elements.forEach(element => {
    this.renderElement(element)
  })
}

function renderElement(element) {
  // присвоить штуки и засунуть в локал стор
  let post = document.querySelector('#post');
  let postInput = document.querySelector('.post-input');
  let postText = post.content.querySelector('.post-main-text');
  postText.textContent = postInput.value;
  let content = document.querySelector('.content');


  const clone = document.importNode(post.content, true);
  content.appendChild(clone);
  localStorage.setItem('elements', JSON.stringify(elements));
}

renderSomething();



// проходим по массиву (функции в которой закидываем данные в этот массив)













